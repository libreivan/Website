const readingTime = require('eleventy-plugin-reading-time');
const pluginRss = require("@11ty/eleventy-plugin-rss");

module.exports = function(eleventyConfig) {
	eleventyConfig.addPlugin(readingTime);
	eleventyConfig.addPlugin(pluginRss);

	  // Copy `src/style.css` to `_site/style.css`
	eleventyConfig.addPassthroughCopy("src/style.css");

	// Set custom directories for input, output, includes, and data
	return {
	    // When a passthrough file is modified, rebuild the pages:
		passthroughFileCopy: true,
		dir: {
			input: "src",
			includes: "_includes",
			data: "_data",
			output: "_site"
		}
	};
};
